# Clipboard ![Docker Image Size (tag)](https://img.shields.io/docker/image-size/bradbot1/clipboard/latest) ![Docker Stars](https://img.shields.io/docker/stars/bradbot1/clipboard) ![Docker Pulls](https://img.shields.io/docker/pulls/bradbot1/clipboard)

A simple static HTTP site that applies a provided string to the users clipboard.

## Why does this exist?

It exists so that Sponge7 (minecraft mod framework) ore can send clients to the site in order to replicate sponge10's copy to clipboard functionality.

## How to use

Send the user to a hosted version of index.html with the text to be applied as the parameter.

For instance, requesting the URL `https://example.com?u=this%20is%20a%20test` will result in the text `this is a test` being placed upon the clipboard.

### Parsing Flags

There are three prefixes that can be used to customise the parsing of the url:

1. u
2. b
3. r

#### u - URI

Decodes URI encoded characters via the `decodeURI()` function


The following results in the text `copy this` being placed upon the clipboard:
```
https://example.com?u=copy%20this
```

#### b - Base64

Decodes the base64 encoded characters via the `atob()` function

The following results in the text `copy this` being placed upon the clipboard:
```
https://example.com?b=Y29weSB0aGlz
```

#### r - Raw

No decoding occurs

The following results in the text `copy%20this` being placed upon the clipboard:
```
https://example.com?r=copy this
```

## Edge Cases

### No JavaScript

Clipboard relies upon JavaScript to grab and then parse the provided query from the users URL, without it a simple message stating that they should enable javascript in order to get functionality is is provided.

### No JavaScript Clipboard API

If the browser has no clipboard api a message requesting for them to enable it is provided.

Below this message is the contents that were to be placed inside the clipboard so that they can manually copy said message.

### No Permission

If the write to the clipboard fails a message stating that permission is needed and the contents that would have been placed in the clipboard are displayed.